module Port
  IDENTIFIER = :PALE

  def self.get_identifier(port_name)
    port_name[0, 4].upcase.to_sym
  end

  def self.get_terminal(ship_identifier)
    cargo_type = ship_identifier[0, 3]

    if %w[OIL GAS].include?(cargo_type)
      :A
    else
      :B
    end
  end
end
