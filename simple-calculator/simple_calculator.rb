class SimpleCalculator
  ALLOWED_OPERATIONS = %w[+ / *].freeze

  def self.calculate(first_operand, second_operand, operation)
    case operation
    when '+'
      out(first_operand, second_operand, operation, first_operand + second_operand)
    when '/'
      return 'Division by zero is not allowed.' if second_operand.zero?

      out(first_operand, second_operand, operation, first_operand / second_operand)
    when '*'
      out(first_operand, second_operand, operation, first_operand * second_operand)
    else
      raise UnsupportedOperation
    end
  rescue TypeError => e
    raise ArgumentError
  end

  def self.out(first, sec, opr, res)
    "#{first} #{opr} #{sec} = #{res}"
  end

  class UnsupportedOperation < ArgumentError
  end
end

