class LogLineParser
  def initialize(line)
    @line = line
    @match = @line.match(/^\[([A-Z]+)\]: (.+)/)
  end

  def message
    @match[2].chomp.strip
  end

  def log_level
    @match[1].downcase
  end

  def reformat
    "#{message} (#{log_level})"
  end
end
