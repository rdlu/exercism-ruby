class Attendee
  def initialize(height)
    @height = height
    @pass_id = nil
  end

  attr_reader :height, :pass_id

  def issue_pass!(pass_id)
    @pass_id = pass_id
  end

  def revoke_pass!
    @pass_id = nil
  end

  def has_pass?
    !@pass_id.nil?
  end

  def fits_ride?(required_height)
    @height >= required_height
  end

  def allowed_to_ride?(required_height)
    has_pass? && fits_ride?(required_height)
  end
end
